var express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server),
	crypto = require('crypto'),
	users = [];

server.listen(3000);

// Routes
app.get('/', function(req, res) {
	res.sendfile(__dirname + '/index.html');
});

// All socket code goes in here.
io.sockets.on('connection', function(socket) {

	// Send Messsage event
	socket.on('send message', function(data) {
		// Send message to all users, globally.
		io.sockets.emit('new message', {msg: data, user: JSON.parse(socket.user)});
	});

	// Started Typing event
	socket.on('started typing', function(data) {
		io.sockets.emit('user started typing', {user: JSON.parse(socket.user)});
	});

	// Done Typing event
	socket.on('done typing', function(data) {
		io.sockets.emit('user done typing', {user: JSON.parse(socket.user)})
	});

	// New User event
	socket.on('new user', function(first, email, callback) {
		// Hash the user's email
		var md5 = crypto.createHash('md5');
		md5.update(email);
		var emailhash = md5.digest('hex');
		// Set up a user object.
		var user = {"name": first, "email": email, "emailhash": emailhash};
		// If the user is in the users array, fail.
		if (users.indexOf(JSON.stringify(user)) !== -1) {
			callback(false);
			console.log('User' + first + ' already exists');
		}
		// If not, let the user in.
		else {
			// Send back the email hash with the callback.
			callback(emailhash);
			console.log('User not here yet; allowing in');
			socket.user = JSON.stringify(user);
			// Add the user to the users array.
			users.push(socket.user);
			// Send updated users list to all users, globally.
			updateUsers();
		}
	});

	function updateUsers() {
		io.sockets.emit('usernames', users);
	}

	// User disconnects
	socket.on('disconnect', function(data) {
		if(!socket.user) return;
		users.splice(users.indexOf(socket.user), 1);
		updateUsers();
	});
});